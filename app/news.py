import requests, json
from bs4 import BeautifulSoup

def news():
    url = requests.get("http://www.aljazeera.com/news/")
    print(url)

    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []

    main_content = soup.find('div', {'class': 'top-topics-wrapper'})
    frame = main_content.find('div', {'class': 'top-section-lt'})
    print("section1")
    # frame = content.find('div', {'class': 'top-section-lt'})
    inner_frame = frame.find('div', {'class': 'frame-container'})
    link = inner_frame.find('a')
    link_href = "https://www.aljazeera.com"+link.get('href')

    image = link.find('img', {'class': 'img-responsive'})
    image_src = "https://www.aljazeera.com"+image.get('src')

    inner_frame2 = frame.find('div', {'class': 'top-feature-overlay-cont'})
    heading = inner_frame2.find('h2', {'class': 'top-sec-title'})

    paragraph = inner_frame2.find('p', {'class': 'top-sec-desc'})

    main_list.append({"link": link_href, "image": image_src, "heading": heading.text, "paragraph": paragraph.text, "author":"", "time":"", "category":""})

    ############ section2 #################
    print('section2')
    content = soup.find('div', {'class': 'col-md-6 middle-east-bot'})

    frames = content.find_all('div', {'class': 'col-md-6 middleEast-rt-topic-wrap default-style default-style'})
    for frame in frames:
        inner_frame = frame.find('div', {'class': 'frame-container'})
        link = inner_frame.find('a')
        link_href = "https://www.aljazeera.com"+link.get('href')

        image = link.find('img', {'class': 'img-responsive'})
        image_src = "https://www.aljazeera.com"+image.get('src')

        inner_frame = frame.find('div', {'class': 'topFeature-sblock-wr'})
        heading = inner_frame.find('h2', {'class': 'top-sec-smalltitle'})
        main_list.append({"link": link_href, "image": image_src, "heading": heading.text, "paragraph": "", "author":"", "time":"", "category":""})

    ##############  Section3 #################

    print('section3')

    section_block = soup.find_all('div', {'class': 'topics-sec-block'})
    for section in section_block:
        section_row = section.find_all('div', {'class': 'row topics-sec-item default-style'})
        for row in section_row:
            time = row.find('time', {'id': 'PubTime'})
            category = row.find('a', {'class':'topics-sec-item-label'}).text
            link = row.find('a', {'class': None})
            link_href = "https://www.aljazeera.com"+link.get('href')
            heading = row.find('h2', {'class': 'topics-sec-item-head'})
            para = row.find('p', {'class': 'topics-sec-item-p'})
            image = row.find('img', {'class': 'img-responsive lazy'})
            image_src = "https://www.aljazeera.com"+image['src']
            main_list.append({"link": link_href, "image": image_src, "heading": heading.text, "paragraph": para.text, "author":"", "time":time.text, "categpry":category})

    opinion_block = soup.find_all('div', {'class': 'col-lg-12 col-md-3 col-xs-6 topics-sidebar-opinion-item-wrap'})
    for opinion in opinion_block:
        link = opinion.find('a', {'class': 'topics-sidebar-title'})
        link_href = "https://www.aljazeera.com"+link.get('href')
        heading = opinion.find('h3')
        image = opinion.find('img', {'class': 'opinion-author-img-mobile'})
        img_src = "https://www.aljazeera.com"+image.get('src')
        author = image.get('title')
        main_list.append({"link": link_href, "image": img_src, "heading": heading.text, "paragraph": "", "author": author, "time":"", "category":""})

    featured_block = soup.find_all('div', {'class': 'col-lg-12 col-md-3 col-xs-6 topics-sidebar-indepth-item'})
    for featured in featured_block:
        link = featured.find('a')
        link_href = "https://www.aljazeera.com"+link.get('href')
        heading = featured.find('h2', {'class': 'indepth-inner-title'})
        image = featured.find('img', {'class': 'indepth-img img-responsive'})
        img_src = "https://www.aljazeera.com"+image.get('src')
        category = featured.find('h4', {"class":"indepth-feature"}).text
        main_list.append({"link": link_href, "image": img_src, "heading": heading.text, "paragraph": "", "author":"", "time":"", "category":category})
    return main_list


def news_content(url):
    url = requests.get(url)
    print(url)
    content = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    region = content.find('div', {"class": "article-top-label"})
    region = region.find('li', {'class':"mini-secondary"})
    if region:
        category = region.text
    else:
        category = ""
    heading = content.find('h1', {"class": "post-title"})
    if heading:
        title = heading.text
    else:
        title = ""
    para = content.find('p', {"class":"article-heading-des"})
    if para:
        desc = para.text
    else:
        desc = ""
    author = content.find('div', {"class": "article-heading-author-name"})
    author_name = author.find('a')
    if author_name:
        name = author_name.text
    else:
        name = ""
    time = author.find('time')
    if time:
        news_time = time.text
    else:
        news_time = ""
    article_body = content.find("div", {"class": "main-article-body"})
    article_content = article_body.find('div', {"class": "article-p-wrapper"})
    extract_div = article_content.find_all('div')
    if extract_div:
        for e in extract_div:
            e.extract()
    article = article_content.get_text()
    article = article.replace('\n', "")
    article = article.replace('\"', "")
    main_list.append({"category": category, "content": article, "summary": desc, "author":name, "time": news_time})
    return main_list


