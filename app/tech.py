import requests, json
from bs4 import BeautifulSoup

def tech_news():
    url = requests.get("https://www.cnet.com/news/")
    print(url)

    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []

    feed_card = soup.find_all("div", {"class":"riverPost"})
    for feed in feed_card:
        if len(feed["class"]) != 1:
            continue
        time = feed.find("div", {"class":"timeAgo"})
        if time:
            time = time.text.replace('\n', "").replace('\"', "")
        else:
            time = ""
        category = feed.find("a", {"class": "topicName"})
        if category:
            category = category.text.replace('\n', "").replace('\"', "")
        else:
            category = ""
        image = feed.find("img")
        if image:
            image=  image.get("data-original")
        else:
            image = ""
        heading = feed.find("h3")
        if heading:
            title = heading.text.replace('\n', "").replace('\"', "")
        else:
            title = ""
        link = heading.find("a", {"class":"assetHed"})
        if link:
            link = "https://www.cnet.com/"+link.get("href")
        else:
            link = ""
        author = feed.find("span", {"class":"assetAuthor"})
        if author:
            author = author.text
        else:
            author = ""
        paragraph = feed.find('p')
        if paragraph:
            paragraph = paragraph.text.replace('\n', "").replace('\"', "")
        else:
            paragraph = ""
        main_list.append({"link": link, "image": image, "heading": title, "paragraph": paragraph, "author":author, "time":time, "category":category})
    return main_list

def tech_content(url):
    url = requests.get(url)
    print(url)
    body = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    content = body.find("div", {"class":"col-7 article-main-body row"})
    extract_div = content.find_all('figure')
    if extract_div:
        for e in extract_div:
            e.extract()
    paragraph = content.text.replace('\n', "").replace('\"', "")
    main_list.append({"content":paragraph})
    return main_list