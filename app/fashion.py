import requests, json
from bs4 import BeautifulSoup

def fashion_news():
    url = requests.get("https://www.vogue.com/fashion")
    print(url)

    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []

    feed_card = soup.find_all("div", {"class":"feed-card--wrapper"})
    for feed in feed_card:
        time = feed.find("time", {"class":"feed-card--time"})
        if time:
            time = time.text
        else:
            time = ""
        category = feed.find("h3", {"class": "feed-card--category"})
        if category:
            category = category.text
        else:
            category = ""
        image = feed.find("img", {"class":"collection-list--image"})
        if image:
            image=  image.get("srcset")
        else:
            image = ""
        heading = feed.find("h2",{"class":"feed-card--title"})
        if heading:
            title = heading.text
        else:
            title = ""
        link = heading.find("a")
        if link:
            link = link.get("href")
        else:
            link = ""
        author = feed.find("a", {"class":"contributor-byline--name"})
        if author:
            author = author.text
        else:
            author = ""
        main_list.append({"link": link, "image": image, "heading": title, "paragraph": "", "author":author, "time":time, "category":category})
    return main_list

def fashion_content(url):
    url = requests.get(url)
    print(url)
    content = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    content = content.find("div", {"class":"article-copy--body"})
    content = content.find_all("p")
    paragraph = " "
    for para in content:
        print(para.text)
        paragraph += para.text
    main_list.append({"content":paragraph})
    return main_list