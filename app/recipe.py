import requests, json
from bs4 import BeautifulSoup

def recipes():
    base_url = "https://www.masala.tv/recipes/"
    url = requests.get(base_url)
    print(url)

    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []

    feed_card = soup.find_all("article", {"class":"item-list"})
    for feed in feed_card:
        if len(feed["class"]) != 1:
            continue
        time = ""
        category = ""
        image = feed.find("img")
        if image:
            image=  image.get("src")
        else:
            image = ""
        heading = feed.find("h3",{"class":"cat_title"})
        if heading:
            title = heading.text.replace('\n', "").replace('\"', "")
        else:
            title = ""
        link = feed.find("a")
        if link:
            link = link.get("href")
        else:
            link = ""
        author = ""
        paragraph = ""
        main_list.append({"link": link, "image": image, "heading": title, "paragraph": paragraph, "author":author, "time":time, "category":category})

    for i in range(2, 20):
        url = requests.get(base_url+"page/"+str(i)+"/")
        print(url)

        soup = BeautifulSoup(url.content, 'html.parser')

        feed_card = soup.find_all("article", {"class": "item-list"})
        for feed in feed_card:
            if len(feed["class"]) != 1:
                continue
            time = ""
            category = ""
            image = feed.find("img")
            if image:
                image = image.get("src")
            else:
                image = ""
            heading = feed.find("h3", {"class": "cat_title"})
            if heading:
                title = heading.text.replace('\n', "").replace('\"', "")
            else:
                title = ""
            link = feed.find("a")
            if link:
                link = link.get("href")
            else:
                link = ""
            author = ""
            paragraph = ""
            main_list.append(
                {"link": link, "image": image, "heading": title, "paragraph": paragraph, "author": author, "time": time,
                 "category": category})
    return main_list

def recipes_content(url):
    url = requests.get(url)
    print(url)
    body = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    global image
    title = body.find("strong")
    if title:
        title = title.text.replace("\n", "").replace("&nbspin;", "").replace("\r", "")
    else:
        title = ""
    info = body.find("ul", {"class":"recipe-info"})
    if info:
        image = ""
        info_steps = [step.text.replace("\n", " ") for step in info.find_all("ul")]
    else:
        info_steps = ""
        recipe_image = body.find("img", {"class":" wp-image-29379 aligncenter"})
        print(recipe_image)
        if recipe_image:
            image = recipe_image.get('src')
        else:
            image = ""
    content = body.find("div", {"class":"recipe_tab_holder"})
    if content:
        ingredients = content.find('div', {"class":"ingredients_div"})
        steps = [step.text for step in ingredients.find_all("li")]
        method = content.find("div", {"class":"method_div"})
        steps2 = [step.text for step in method.find_all("li")]
    else:
        steps = ""
        steps2 = ""
    main_list.append({"info":info_steps, "ingredients":steps, "methods":steps2, "recipe_image": image, "title":title})
    return main_list