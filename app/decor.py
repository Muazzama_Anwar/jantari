import requests, json
from bs4 import BeautifulSoup

def decor_news():
    url = requests.get("http://www.home-designing.com/")
    print(url)

    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []

    feed_card = soup.find_all("div", {"class":"post"})
    for feed in feed_card:
        image = feed.find("img", {"class":"attachment-homepage-thumb size-homepage-thumb wp-post-image"})
        if image:
            image=  image.get("src")
        else:
            image = ""
        heading = feed.find("h2")
        if heading:
            title = heading.text
        else:
            title = ""
        link = heading.find('a')
        print(link)
        if link:
            link = link.get('href')
        else:
            link = ""
        if image:
            main_list.append({"link": link, "image": image, "heading": title, "paragraph": "", "author":"", "time":"", "category":""})
    return main_list

def decor_content(url):
    url = requests.get(url)
    print(url)
    content = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    items = content.find_all("div", {"class":"gallery-item"})
    for item in items:
        image = item.find('img', {'class':'attachment-thumbnail'})
        if image:
            image = image.get('src')
        else:
            image = ""
        caption = item.find('dd', {'class':'gallery-caption'})
        if caption:
            caption = caption.text
        else:
            caption = ""
        main_list.append({"image":image, "caption":caption})
    return main_list