import requests, json
from bs4 import BeautifulSoup

def sports_news():
    url = requests.get("https://www.dawn.com/sport")
    print(url)
    soup = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    top_story = soup.find('article', {'class':'box story story__top mb-2 pr-2 lap-and-up-border--right'})
    author = top_story.find('span', {'class': 'story__byline size-two caps text--gray '})
    if author:
        author = author.text
    else:
        author = ""
    image = top_story.find('img')
    if image:
        image = image.get('src')
    else:
        image = ""
    link = top_story.find('a')
    if link:
        link = link.get('href')
    else:
        link = ""
    heading = top_story.find('h2')
    if heading:
        heading = heading.text
    else:
        heading = ""
    summary = top_story.find('div', {'class':'story__excerpt text-center my-2 mb-sm-4 mt-sm-2'})
    if summary:
        summary = summary.text
    else:
        summary = ""
    time = top_story.find('span', {'class':'timestamp--time timeago'})
    if time:
        time = time.text
    else:
        time = ""
    main_list.append({"author":author, "time":time, "image":image, "heading":heading, "paragraph":summary, "link":link, "category":category})
    stories = soup.find_all('div', {'class':'col-sm-3 col-12'})
    for story in stories[1:]:
        author = story.find('span', {'class':'story__byline size-two caps text--gray '})
        if author:
            author = author.text
        else:
            author = ""
        image = story.find('img')
        if image:
            image = image.get('src')
        else:
            image = ""
        link = story.find('a')
        if link:
            link = link.get('href')
        else:
            link = ""
        heading = story.find('h2')
        if heading:
            heading = heading.text
            heading = heading.replace('\n', '')
            heading = heading.replace('\"', '')
        else:
            heading = ""
        summary = story.find_all('div')
        if summary:
            summary = summary[2].text
            summary = summary.replace('\n', '')
            summary = summary.replace('\"', '')
        else:
            summary = ""
        time = story.find('span', {'class': 'timestamp--time timeago'})
        if time:
            time = time.text
        else:
            time = ""
        main_list.append({"author": author, "time": time, "image": image, "category": "", "heading": heading, "paragraph": summary,
                          "link": link})
    return main_list


def sports_content(url):
    url = requests.get(url)
    print(url)
    content = BeautifulSoup(url.content, 'html.parser')
    main_list = []
    header = content.find('div', {'class':'template__header clearfix modifier--open px-1 '})
    heading = header.find('h2')
    if heading:
        heading = heading.text
        heading = heading.replace('\n', "")
        heading = heading.replace('\"', "")
    else:
        heading = ""
    time = header.find('span', {'class':'story__time font--arial '})
    if time:
        time = time.text
    else:
        time = ""
    author = header.find('span', {'class':'story__byline font--arial '})
    if author:
        author = author.text
    else:
        author = ""
    image = header.find('img')
    if image:
        image = image.get('src')
    else:
        image = ""
    body = content.find('div', {'class':'template__main clearfix modifier--open px-1 modifier--widedefault modifier--blockquote-center-narrow '})
    ad = body.find_all('div')
    if len(ad)>1:
        ad[1].extract()
    paragrapgh = body.get_text()
    paragrapgh = paragrapgh.replace('\n', "")
    paragrapgh = paragrapgh.replace('\"', "")
    main_list.append({"content": paragrapgh})
    return main_list