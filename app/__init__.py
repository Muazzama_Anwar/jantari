import requests, json
from bs4 import BeautifulSoup
from flask import Flask, jsonify, url_for, redirect, request
from app import news, sports, fashion, decor, tech, recipe

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect(url_for('news_page'))

@app.route('/news', methods=['GET', 'POST'])
def news_page():
    return jsonify({"result": news.news(), "status": 1})

@app.route('/news_content', methods=['GET', 'POST'])
def news_content_page():
    print(request.args.get('url'))
    return jsonify({"result":news.news_content(request.args.get('url')), "status":1})

@app.route('/sports')
def sports_page():
    return jsonify({"result":sports.sports_news(), "status":1})

@app.route('/sports_content', methods=['GET', 'POST'])
def sports_content_page():
    print(request.args.get('url'))
    return jsonify({"result":sports.sports_content(request.args.get('url')), "status":1})

@app.route('/fashion')
def fashion_page():
    return jsonify({"result":fashion.fashion_news(), "status":1})

@app.route('/fashion_content', methods=['GET', 'POST'])
def fashion_content_page():
    print(request.args.get('url'))
    return jsonify({"result":fashion.fashion_content(request.args.get('url')), "status":1})

@app.route('/decor', methods=['GET', 'POST'])
def decore_page():
    return jsonify({"result":decor.decor_news(), "status":1})

@app.route('/decor_content', methods=['GET', 'POST'])
def decor_content_page():
    print(request.args.get('url'))
    return jsonify({"result":decor.decor_content(request.args.get('url')), "status":1})

@app.route('/tech', methods=['GET', 'POST'])
def tech_page():
    return jsonify({"result":tech.tech_news(), "status":1})

@app.route('/tech_content', methods=['GET', 'POST'])
def tech_content_page():
    print(request.args.get('url'))
    return jsonify({"result":tech.tech_content(request.args.get('url')), "status":1})

@app.route('/recipes', methods=['GET', 'POST'])
def recipe_page():
    return jsonify({"result":recipe.recipes(), "status":1})

@app.route('/recipes_content', methods=['GET', 'POST'])
def recipes_content_page():
    print(request.args.get('url'))
    return jsonify({"result":recipe.recipes_content(request.args.get('url')), "status":1})
